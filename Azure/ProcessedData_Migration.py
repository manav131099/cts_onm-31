import requests, json
import requests.auth
import pandas as pd
import datetime as dt
import os
import re 
import time
import shutil
import pytz
import sys
import numpy as np
import logging
import pyodbc
        
###Changes Start###
sourcesiteid = '181'
destinationsiteid = '4'
componentid = '91'
###Changes End###

#Connecting to server
server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 13 for SQL Server}'
connStr = pyodbc.connect('DRIVER='+driver+'; PORT=1433; SERVER='+server+'; PORT=1443; DATABASE='+database+'; UID='+username+'; PWD='+password)

#Fetching data from source table for the required source site id
source_data = pd.read_sql_query("SELECT [Date], [GHI], [Eac-MFM], [LastR-MFM], [Yield], [PR], [Master-Flag] FROM Stations_Data where Station_Id=" + sourcesiteid, connStr)
source_data.columns = ['Date', 'GHI', 'EAC method-2', 'Last recorded value', 'Yield-2', 'PR-2 (GHI)', 'BypassFlag']
source_data['Date'] = source_data['Date'].dt.date
print(source_data)

#Mapping ParameterName to ParameterID
parameter_mapping = {'GHI':17, 'EAC method-2':15, 'Last recorded value':18, 'Yield-2':20, 'PR-2 (GHI)':44}

#Preparing export table
final = pd.DataFrame(columns = ['Date', 'Value', 'ParameterId', 'ComponentId', 'SiteId', 'BypassFlag']) 
for i in range(len(source_data)):
  for j in parameter_mapping:
    final = final.append({'Date' : source_data['Date'][i],
                          'Value' : source_data[j][i],
                          'ParameterId' : parameter_mapping[j],
                          'ComponentId' : int(componentid),
                          'SiteId' : int(destinationsiteid),
                          'BypassFlag' : source_data['BypassFlag'][i]},
                          ignore_index = True)
print(final)

#Upsert Function
def upsert(Date, Value, ParameterId, ComponentId, SiteId, BypassFlag):
  cursor = connStr.cursor()
  with cursor.execute("UPDATE [All].[ProcessedData] SET [Value]=?, [ParameterId]=?, [ComponentId]=?, [SiteId]=?, [BypassFlag]=? where [Date]=? AND [ComponentId]=? AND [ParameterId]=? if @@ROWCOUNT = 0 INSERT into [All].[ProcessedData]([Date], [Value], [ParameterId], [ComponentId], [SiteId], [BypassFlag]) values(?, ?, ?, ?, ?, ?)", [Value, ParameterId, ComponentId, SiteId, BypassFlag, Date, ComponentId, ParameterId, Date, Value, ParameterId, ComponentId, SiteId, BypassFlag]):
    pass
  connStr.commit()

#Upserting Data to the destination table with destination site id
for i in range(len(final)):
  upsert(final['Date'][i], final['Value'][i], final['ParameterId'][i], final['ComponentId'][i], final['SiteId'][i], final['BypassFlag'][i]) 

#Disconnecting Server
connStr.close()