source('C:/Users/talki/Desktop/cec intern/codes/PH-002X/[PH-002X] Data_extract.R')

rm(list=ls(all =TRUE))
library(ggplot2)

pathWrite <- "C:/Users/talki/Desktop/cec intern/results/PH-002X/"
result <- read.csv("C:/Users/talki/Desktop/cec intern/results/PH-002X/[PH-002X]_summary.csv")
graph3 <- "C:/Users/talki/Desktop/cec intern/results/PH-002X/PH-002X Daily average Tamb.pdf"

#manually add in x axis labels for last graph (every 3 months)
monthslabel <- c("Dec","Mar","Jun","Sept","Dec","Mar","Jun")

rownames(result) <- NULL
result <- data.frame(result)
#result <- result[-length(result[,1]),]
result <- result[,-1]
#result <- result[-1,]
colnames(result) <- c("date","da","pts","gsi","tamb","hamb")
result[,1] <- as.Date(result[,1], origin = result[,1][1])
result[,2] <- as.numeric(paste(result[,2]))
result[,3] <- as.numeric(paste(result[,3]))
result[,4] <- as.numeric(paste(result[,4]))
result[,5] <- as.numeric(paste(result[,5]))
result[,6] <- as.numeric(paste(result[,6]))

LTA <- 1863  #longtermaverage
LTD <- 5.10  #longterm daily

#gsi in the first day is an error
result[1,4] <- 0

date <- result[,1]

dagraph <- ggplot(result, aes(x=date,y=da))+ylab("Data Availability [%]")
da1 <- dagraph + geom_line(size=0.5)
da1 <- da1 + theme_bw()
da1 <- da1 +  expand_limits(x=date[1],y=0)
da1 <- da1 +  scale_y_continuous(breaks=seq(0, 115, 10))
da1 <- da1 +  scale_x_date(date_breaks = "1 month",date_labels = "%b")
da1 <- da1 +  ggtitle(paste("[PH-002X] Manila Data Availability"), subtitle = paste0("From ",date[1]," to ",date[length(date)]))
da1 <- da1 +  theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))
da1 <- da1 +  theme(axis.title.x = element_blank())
da1 <- da1 +  theme(axis.text.x = element_text(size=11))
da1 <- da1 +  theme(axis.text.y = element_text(size=11), plot.subtitle = element_text(face = "bold",size= 12,lineheight = 0.9,hjust = 0.5))
da1 <- da1 +  theme(plot.title = element_text(face = "bold",size= 12,lineheight = 0.7,hjust = 0.5))
da1 <- da1 +  theme(plot.title=element_text(margin=margin(0,0,7,0)), panel.grid.minor = element_blank())
da1 <- da1 +  annotate("text",label = paste0("Average data availability = ", round(mean(result[,2]),1),"%"),size=3.6,
           x = as.Date(date[round(0.518*length(date))]), y= 112)
da1 <- da1 +  annotate("text",label = paste0("Lifetime = ", length(date)," days (",format(round(length(date)/365,1),nsmall = 1)," years)    "),size = 3.6,
           x = as.Date(date[round(0.518*length(date))]), y= 107)
da1 <- da1 +  theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm"))#top,right,bottom,left

da1
ggsave(da1,filename = paste0(pathWrite,"PH-002_DA_LC.pdf"),width =7.92, height =5)

result[,4][result[,4] > 10] <- NA

gsiGraph <- ggplot(result, aes(x=date,y=gsi))+ylab("GHI [W/m^2]")
gs1 <- gsiGraph + geom_bar(stat = "identity", width = 1, position = "dodge")
gs1 <- gs1 +  theme_bw()
gs1 <- gs1 +   expand_limits(x=date[1],y=10)
gs1 <- gs1 +   scale_y_continuous(breaks=seq(0, 10, 1))
gs1 <- gs1 +   scale_x_date(date_breaks = "1 month",date_labels = "%b")
gs1 <- gs1 +   ggtitle(paste("[PH-002X] Global Horizontal Irradiation Daily"), subtitle = paste0("From ",date[1]," to ",date[length(date)]))
gs1 <- gs1 +   theme(axis.title.y = element_text(face = "bold",size = 12,margin = margin(0,2,0,0)))
gs1 <- gs1 +   theme(axis.title.x = element_blank())
gs1 <- gs1 +   theme(axis.text.x = element_text(size=11))
gs1 <- gs1 +   theme(axis.text.y = element_text(size=11), plot.subtitle = element_text(face = "bold",size= 12,lineheight = 0.9,hjust = 0.5)) 
gs1 <- gs1 +   theme(plot.title = element_text(face = "bold",size= 12, lineheight = 0.8, hjust = 0.5))
gs1 <- gs1 +   theme(plot.title=element_text(margin=margin(0,0,7,0)), panel.grid.minor = element_blank())
gs1 <- gs1 +   geom_hline(yintercept=mean(result[,4][!is.na(result[,6])]),size=0.3,color = "blue")
gs1 <- gs1 +   geom_hline(yintercept=LTD,size=0.3, color="blue")
gs1 <- gs1 +   annotate("text",label = paste0("Long-term average annual GHI = ",LTA," kWh/m^2   "),size=3.6,
           x = as.Date(date[round(0.518*length(date))]), y= 9.5)
gs1 <- gs1 +   annotate("text",label = paste0("Long-term average daily GHI =",LTD," kWh/m^2.day"),size=3.6,
           x = as.Date(date[round(0.518*length(date))]), y= 9.0, color="blue")
gs1 <- gs1 +   annotate("text",label = paste0("Current average daily GHI = ",round(mean(result[,4][!is.na(result[,4])]),2)," kWh/m^2.day"),size=3.6,
           x = as.Date(date[round(0.518*length(date))]), y= 8.5, color="red")
gs1 <- gs1 +   geom_hline(yintercept=round(mean(result[,4],na.rm=TRUE),2), size=0.3, color="red")
gs1 <- gs1 +   theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm"))
gs1

ggsave(gs1,filename = paste0(pathWrite,"PH-002_GSI_LC.pdf"),width =7.92, height =5)

pdf(graph3,width=7.92,height=5)

tamb <- result[,5]

tamb[tamb <1.5] = NA
yaxis <- tamb

xaxis <- c(1:length(yaxis))
xaxis2 <- seq(30,(length(yaxis)),80)   #-11

par(mar=c(3.5, 4, 3, 4) + 0.1)

## Plot first set of data and draw its axis
plot(xaxis, tamb, pch=4, axes=FALSE, ylim=c(-100,75), xlab="", ylab="", 
     type="l",col="orange", main=" ")
axis(2, ylim=c(-100,75),at = seq(-100,75,25),col="black",col.axis="black",las=1)
mtext(expression(bold("Tamb [ C]")),side=2,line=2.5,cex=1,las =3)

box()

title(paste("[PH-002X] Daily Average Ambient Temperature \n From ", date[1], " to ", date[length(date)]), col="black", cex.main =1)

#mtext(expression(paste(bold("[PH-002X] Daily Average Ambient Temperature"))),side=3,col="black",line=1.75,cex=1) 
#print(date[length(date)])
#mtext(expression(paste(bold("From 2016-12-18 to 2018-06-09"))),side=3,col="black",line=0.75,cex=1)

text(length(xaxis)/2, 60, paste("Average Tamb =",format(round(mean(tamb[abs(tamb)<100&!is.na(tamb)]),1),nsmall = 1),"C"),cex = .8,col = "orange")


#axis(1,at = xaxis2,cex.axis = .8,labels = c("Dec","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept", "Oct","Nov","Dec","Jan","Feb","Mar","Apr","May","Jun"))
axis(1,at = xaxis2,cex.axis = .8,labels = monthslabel)

par(new=TRUE)

dev.off()

print(paste0("Graphs located @  ",pathWrite))

