# Documentation of Stations running on TORP systems
The tree structure of the repository looks like this
* [Station Name]("~/CODE/[IN-002T]")
  - 2G3GFunction.R
  - Mail.R 
  - Live
  - Historical.R
  - Functions.R
 
<br>The system is divided in 5 stages : 
* 2G3GFunction
* Mail
* Live
* Historical Analysis
* Functions 
 
</br>
The control flow of the whole operation goes like this:
<br>
```
Mail  					Live  				Historical
 |		   				  |		         		|
 Sources                  Source                Sources
 1. 2G3GFunction.R        1. Functions.R        1.Functions.R
 2. send_mail.R           2. send_mail.R        |
 |                        |                     |
 |                        |                     |
 Job                      Job                   Job
 1. Creates 2nd, 3rd      1. Creates 1st-Gen    1. Creates historical
    Gen data                 from raw data         data for particular
 2. Sends out Mail        2. Sends out Live        months
    digest                   alert & mail
```
The above 5 systems are described below for better understanding. 
## 2G3G Functions
``2G3GFunction.R`` ,this part consists of 2 function namely as 
* Second Gen
* Third Gen

### Second Gen 
```
secondGenData = function(path,pathw,mtno)
```
This function creates the *Gen-2* data present in [DropBox]("~DropBox/Second\bGen/"). 
<br> It takes two arguments as *path* to read the 1st Gen data and *pathw* to read to second Gen data.</br>
```
write.table(df,file = pathw,row.names = F,col.names = T,sep = "\t",append = F)
```
Writes the calculated 2nd Gen data to the DropBox. 
```
(path,pathw,mtno)
```
*path , pathw, mtno* are respectively location for reading the raw *Gen 1* data, writing *Gen 2* data and the current month in number.
### Third Gen
```
thirdGenData = function(pathr,pathw)
```

Creates 3rd-Gen data for historical analysis read and written to respectively to location in *pathr* and *pathw*.
```
timetomins = function(x)
```
Convert time in format HH:MM to a numerical value between 1-1140
## Mail 
``Mail.R`` Script used to send daily mail digest at arrival of new data. 
<br> This section is divided is 2 parts as following :
1. ``sendmail()``
2. `` getRecipients()``
```
sendMail= function(path)
```
``sendMail()`` creates the mail body from second gen data located at location *path*.
This script also sources the earlier mention script ``2G3GFunction.R`` to create the second Gens data. Below is an example for path location for site 'IN-001T' : <br>
<br>
``path = "/home/admin/Dropbox/Gen 1 Data/[IN-001T]" ``<br>
``path2G = '/home/admin/Dropbox/Second Gen/[IN-001T]'``<br>
``path3G = '/home/admin/Dropbox/Third Gen/[IN-001T]'``<br>
<br> When new data arrives at the FTP server as here: ``"http://52.70.243.223/torp/ServiceRouter/loginloginid=operations@cleantechsolar.com&pwd=********"``, the script runs the *sendMail()* function to create the digest and fire up the mail.
```
 getRecipients("IN-001T","m")
```
``getRecipients()`` takes the required mail recipient from the recipient list for particular site.
First argument is to tell which site to choose,*'IN-001T'*, and the second one ,i.e., *'m'* indicates type of mail whether mail digest or mail alerts described in next section titled '``Live``'.
```
errHandle = file('/home/admin/Logs/LogsIN001Mail.txt',open='w',encoding='UTF-8')
``` 
Stores the log file for particular station.
## Live
``Live.R'' sends live alert messages (via **Twilio**) and e-mails when certain criterias were not met or met in a complimentary situation. 
This section is divided in 3 section:
- Conditions
- Control Flow
- Fucntions
  - firetwilio() 
  - sendEmailToo()
  - SENDWARNINGMAIL()
  - SENDOKMAIL()
  - managebacklog()
  - manageflags()

- Conditions
<br> 

The alert system checks for this condition :
* **Pac Error**

1. Is the last time-stamp between 8am-5pm, if not return
2. Check if ``doneforthedayfiringpac`` flag is not set, if its set(i.e equal to 1) return. 
3. If 1) and 2) are satisfied extract the portion of the Gen-1 data from 
``lastsuccesspacts`` variable to the end of the data-file. 

For example:
		
If lastsuccesspacts is 2016-12-12 10:28:00 and the Gen-1 data is as below:
		
		Tm									Pac	Eac	GHI		Tmod
		2016-12-12 10:23:00	15	994	1024	38.2
		2016-12-12 10:33:00	12	991	1023	38.4
		2016-12-12 10:38:00	11	999	1021	38.0
	
Then extract sub data fro ``2016-12-12 10:33:00`` till the end of file.
Once the data has been extracted update the ``lastsuccesspacts`` variable to the last timestamp of the Gen-1 data (In the above example ``lastsuccesspacts`` will be updated to ``2016-12-12 10:38:00``). Check if the last recorded **Pac** value is less than the threshold (0.5W).
If yes then update the ``lastfiredpacts`` variable to the current time-stamp and set the **doneforthedayfiringpac** flag.
* **PR Error**

1. Is last time-stamp between 8am - 5pm, if not return
2. Check if ``doneforthedayfiringpr`` flag is not set, if it is set(i.e equal to 1)return. (to know more about the ``doneforthedayfiringpr`` function refer to the ``manageflags`` function).
3. Once 1) & 2) are satisfied calculate Live PR and perform following checks
   -   If **live-PR** is > 60 but less than 100, reset prless60 and **prles50** variable to 0.
   - If **live-PR** is < 25 or live-PR > 100 but less than 500, fire a warning message immediately. Once this is done, set the doneforthedayfiringpr flag & update the variable ``lastfiredtspr25`` to the current time. Also reset ``prless50`` and ``prless60`` variables to 0.
   - If **live-PR** is > 25 but live-PR is < 50, increment the ``prless50`` and ``prless60`` variable by 1.
     - If the ``prless50`` variable is greater than 2 fire the warning message and set ``doneforthedayfiringpr`` flag & update the variable ``lastfiredtspr50`` to the current time. Reset the``prless50`` and ``prless60`` variables to 0.
   - If **live-PR** is > 50 but **live-PR** is < 60, increment the ``prless60``
	variable by 1.
     - If the ``prless60`` variable is greater than 4 fire the warning message and set `` doneforthedayfiringpr`` flag & update the variable ``lastfiredtspr60`` to current time. Reset the ``prless60`` `` prless50`` variables to 0.

Based on the above errors a warning message is fired which is done by calling a python script. We pass as arguments to this script the body of the message, which contains the time-stamp along with the corresponding **Pac** reading or the live-PR based on who fires the warning message. Similarly the ``sendmailtoo`` function is also called which fires a mail containing the warning message.
<br>
```
firetwilio = function(day)
```
Functions to handle firing of warning messages in-case of **PR** error or **Pac**  reading error according to above logic.
<br>If both ``doneforthedayfiringpac`` and ``doneforthedayfiringpr`` flags are set return as no warning messages are to be sent.<br>
* **Control Flow**

- check for data between 9am - 4pm
  - if no values are between this time-window return 
  ```
  print(paste('Not yettime...Lasttime:',as.character(datareadac[nrow(datareadac),1])))
  ```
- extract ``subdata`` from index of ``lastsuccesspacts`` to the end of the file if the match is the last row, then no new data exists so return the call
Extract the sub-data.

- if the match is the last row, then no new data exists so return the call
```
        if((idxtsmtch+1) > nrow(dataread))
		{
			print('No new data recorded.... returning twilio call')
			return()
		}
```
``pacneg`` stores values of pac in the data which are not finite like NA on NaN	
```
	pacneg = pacac[!is.finite(pacac)] 
```
Perform all Pac checking operations only if the ``doneforthedayfiringpac`` flag isn't set.
- Body of the text message
```
message = paste("Station IN-001T Pac meter error, Pac reading:",as.character(pac2[1]),"Timestamp:",as.character(dataread[idx,1]))
command = paste('python /home/admin/CODE/Misc/IN_001.py "',message,'"')
```
				
- System command call to call the python script to fire the text-message
```
system(command)
recordTimeMaster("IN-001T","TwilioAlert",as.character(dataread[idx,1]))
```				
- Send an email too with the message and subject passed as parameters
```				
  sendEmailToo(message,"IN-001T Pac Meter error");
```		
- Set the flag	
```
doneforthedayfiringpac <<-1
print(paste('Pac less than threshold error, message fired, time',as.character(dataread[idx,1])))
```				
- Update ``lastfiredtspac``	
```				
 lastfiredtspac <<- Sys.time()
```
- To send the twilio alert messages, via twilio Python Client

```
command = paste('python /home/admin/CODE/Misc/IN_001.py "',message,'"')
```
<br>Source ``Function.R`` for ``fetchrawdata`` and `` cleansedata`` function calls , in the next section titled ``Function`` , mentioned why.
```source('/home/admin/CODE/IN001Digest/Functions.R')```

```
sendEmailToo = function(message,subj)
```
Function to send a mail along with the ``TWILIO`` message alert. 
The ``firetwilio`` function checks if a warning message has to be sent, if yes this function is called from the ``firetwilio`` function called (defined later in this file), which sends a mail to the recipients.
Function takes inputs message and subject of the mail and returns once mail is sent successfully.

Name of the station
``stationno = "[IN-001T]"``

The last day which was read from the **TORP** server is stored onto a file.
**pathlastday** read stores this path.
``pathlastday = "/home/admin/Start/IN001.txt"``

Path to store the raw-files
``pathraw = paste("/home/admin/Data/TORP Data",stationno,sep="/")``

Path for the gen-1 data
``pathgen1 = paste("/home/admin/Dropbox/Gen 1 Data",stationno,sep="/")``

Flags used by the ``twilio`` function to check if a warning has to be sent each flag is explained in great detail in the ``firetwilio`` function call
``doneforthedayfiringpac = doneforthedayfiringpr = prless50 = prless60 = 0
lastfiredtspac = lastfiredtspr25 = lastfiredtspr50 = lastfiredtspr60  = " "``

Path to store last time-stamp of day for which warning mails and text was sent
``pathtstwilioread = "/home/admin/TwilioStart/IN-001.txt"``

```
SENDWARNINGMAIL = function()
```
Function to send a warning that the FTP server for **TORP** is down. This is triggered if the ``'cleansedata'`` function call returns a null data-frame
after **10** consecutive calls. The ``NODATACOUNTER`` is incremented by 1 every-time  the ``cleansedata`` call returns 0 rows. When this value becomes 10 the mail is fired. This mail is fired at max once a day and sets the ``DATASENTFORTHEDAY`` flag which is only reset when a new day is encountered.
```
SENDOKMAIL = function()
```
This function sends a mail if the **TORP** server is back running once it was 
flagged down. The mail is sent only when the ``SENDWARNINGMAIL`` function call was  called on the same day and when the **TORP** server is pumping back data.
```
if(file.exists(pathtstwilioread))
{
	tmp = readLines(pathtstwilioread)
	if(length(tmp) && tmp!= " ")
	{
		time = Sys.time()
		time = format(time, tz="Asia/Calcutta",usetz=TRUE)
		if(substr(time,1,10) == substr(tmp,1,10))
		{
		lastsuccesspacts = tmp
		}
	}
}
```
Checks if the file which stores last time a **TWILIO** message was sent is present if it is, read its contents and set ``lastsuccesspacts`` flag to the content of the file.
```
managebacklog = function()
```
Function to manage backlog if any. This function checks for backlog present, i.e if the system was down for sometime, then the function fetches data from the last recorded data the system was up to the current date. The pathlastday variable stores the file-name which contains the last recorded date & timefor which the  system was working and successfully recorded the data. This function compares the current date and time with that of what's stored in the file, and asesses if there is any backlog dates for which data has to be fetched.<br>
For example, if the current date is *12-12-2016* and the contents of the 
``pathlastday`` file are *29-11-2016*, then data from *29-11-2016* to *12-12-2016* is fetched using this function call.
```
manageflags = function()
```
Function to update the flags used the ``firetwilio`` function.The flags ``doneforthedayfiringpac`` and ``doneforthedayfiringpr`` are reset to 0if the
time since they were last set to 1 is greater than 1 hr. The ``lastfiredtspac`` and ``lastfiredtspr`` stores the time-stamp when the last warning messages were sent and thus is used to compare if an hour has elapsed since the flags were set. While reseting the ``doneforthedayfiringpac`` flag the flags ``prless50`` `` prless60`` are 
also reset to 0. 
## Function
Script to define all functions used in Live.R and create the Gen-1 data from FTP server. No use of this variable for now, later will be ammended for FTP warning though
``FIREMAILBACKOFF = 1 ``.

This section is divided in 3 parts as:
- cleansedata()
- probePath()
- probePath()
- fetchrawdat()
```
cleansedata = function(pth) 
```
Function to cleanse raw data and output a table which is eventually stored as
Gen-1 data format. The input for the function(pth) is the path of the raw-file, which is generated using the ``fetchrawdata`` function defined later.
The gen-1 data only needs the value Time, Pac, Eac, GHI and Tmod which are rows 19,18,17,16 and 15 respectively of the raw-data. 
<br>The raw data has a bug such that the alternate rows record the correct values for certain parameters, i.e alternate rows have correct values of Pac,Eac while the other rows have correct values for *GHI* and *Tmod*. Incorrect values are identified by noting the value -1000 for Tmod, i.e if *Tmod* is -1000 for a  particular row, then that row has the correct values for *Eac* and *Pac*. <br>However if the value for Tmod is not -1000 then that row has the correct values of *Tmod* and *GHI*. Notice for both these rows the time-stamp is the same.
<br>

For example, the 15-19th columns of the raw data-frame look something like this:
```
Tmod		GHI		Eac		Pac		Time
1000	    1000	44.6	102.1	11:39
38.5		1002	1000	1000	11:39
1000	    1000	50.4	103.2	11:40
38.2		1001	1000	1000	11:40
```
This function will cleanse the above data to give the below output
```
Tm			Pac		Eac		GHI		Tmod
11:39	   102.1	44.6	1002	38.5
11:40	   103.2	50.4	1001	38.2
```
Now this data-frame which is a cleansed version of the raw data is returned

```
probePath = function(day)
```
Function to ping the TORP server for new-data and return the raw-data frame.
The function takes as inputs day1 and day2 which are the start and end-date.
Usually both these are the same, i.e day1 = day2.
The format for day1, day2 is *DD/MM/YYYY*

```
fetchrawdata = function(day1,day2)
```
Fetch data from FTPSERVER using a http POST-request, extracting only rows with certain ( here for IN-001T, MJ Logistics) tag in column 13.
```
subst = which(stnnames %in% "MJ Logistics")
```
As for TORP sites, data are stored at once, thus needing of slicing the data according to respective site's name present in column no **13**. 
## Historical
```
source('/home/admin/CODE/IN001Digest/Functions.R')
```
Using ``fetchrawdata()`` and ``cleansedata()``, creates historical data for particular months of particular year.


