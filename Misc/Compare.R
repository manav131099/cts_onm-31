rm(list = ls())

cleanData = function(data,outer)
{
	tm = substr(as.character(data[,1]),1,13)
	day = substr(tm[1],1,10)
	uniq = unique(tm)
	idxstart = match(uniq,tm)
	Tm = c()
	c2 = c()
	c3 = c()
	c4 = c()
	c5 = c()
	c6 = c()
	c7 = c()
	c8 = c()
	idxGlobal = 1
	for(x in 1:(length(idxstart)-1))
	{
		datasub1 = as.numeric(data[idxstart[x]:idxstart[(x+1)],idxSERIS[[outer]][1]])
		datasub1 = datasub1[complete.cases(datasub1)]
		c2[idxGlobal] = round(mean(datasub1),1)

		if(length(idxSERIS[[outer]]) > 1)
		{
			datasub2 = as.numeric(data[idxstart[x]:idxstart[(x+1)],idxSERIS[[outer]][2]])
			datasub2 = datasub2[complete.cases(datasub2)]
			c3[idxGlobal] = round(mean(datasub2),1)
		}
		if(length(idxSERIS[[outer]]) > 2)
		{
			datasub3 = as.numeric(data[idxstart[x]:idxstart[(x+1)],idxSERIS[[outer]][3]])
			datasub3 = datasub3[complete.cases(datasub3)]
			c4[idxGlobal] = round(mean(datasub3),1)
		}
		if(length(idxSERIS[[outer]]) > 3)
		{
			datasub4 = as.numeric(data[idxstart[x]:idxstart[(x+1)],idxSERIS[[outer]][4]])
			datasub4 = datasub4[complete.cases(datasub4)]
			c5[idxGlobal] = round(mean(datasub4),1)
		}
		if(length(idxSERIS[[outer]]) > 4)
		{
			datasub5 = as.numeric(data[idxstart[x]:idxstart[(x+1)],idxSERIS[[outer]][5]])
			datasub5 = datasub5[complete.cases(datasub5)]
			c6[idxGlobal] = round(mean(datasub5),1)
		}
		if(length(idxSERIS[[outer]]) > 5)
		{
			datasub6 = as.numeric(data[idxstart[x]:idxstart[(x+1)],idxSERIS[[outer]][6]])
			datasub6 = datasub6[complete.cases(datasub6)]
			c7[idxGlobal] = round(mean(datasub6),1)
		}
		if(length(idxSERIS[[outer]]) > 6)
		{
			datasub7 = as.numeric(data[idxstart[x]:idxstart[(x+1)],idxSERIS[[outer]][7]])
			datasub7 = datasub7[complete.cases(datasub7)]
			c8[idxGlobal] = round(mean(datasub7),1)
		}

		Tm[idxGlobal] = paste(tm[idxstart[x]],":30:00",sep="")
		idxGlobal = idxGlobal + 1
	}
	df = data.frame(Tm=Tm,GSI=c2)
	if(length(idxSERIS[[outer]]) > 1)
		df = data.frame(Tm=Tm,GSI=c2,Gmod=c3)
	if(0){
	if(length(idxSERIS[[outer]]) > 2)
		df = data.frame(Tm=Tm,GSI=c2,Gmod=c3,c4=c4)
	if(length(idxSERIS[[outer]]) > 3)
		df = data.frame(Tm=Tm,GSI=c2,Gmod=c3,c4=c4,c5=c5)
	}
	if(length(idxSERIS[[outer]]) > 4)
	df = data.frame(Tm=Tm,GSI=c2,Gmod=c3,c4=c4,c5=c5,c6=c6,c7=c7,c8=c8)
	return(df)
}

pathSERIS = pathGIS = startGIS = filename = idxSERIS = tiltAz = clnames = vector('list')
pathSERIS[[1]] = '/home/admin/Dropbox/Cleantechsolar/1min/[711]'
pathSERIS[[2]] = '/home/admin/Dropbox/Cleantechsolar/1min/[712]'
pathSERIS[[3]] = '/home/admin/Dropbox/Cleantechsolar/1min/[712]'
pathSERIS[[4]] = '/home/admin/Dropbox/Cleantechsolar/1min/[714]'
pathSERIS[[5]] = '/home/admin/Dropbox/Cleantechsolar/1min/[714]'
pathSERIS[[6]] = '/home/admin/Dropbox/Cleantechsolar/1min/[716]'
pathSERIS[[7]] = '/home/admin/Dropbox/Cleantechsolar/1min/[717]'
pathSERIS[[8]] = '/home/admin/Dropbox/Cleantechsolar/1min/[719]'

pathGIS[[1]] = '/home/admin/Dropbox/GIS/Gen-1/NewDelhi'
pathGIS[[2]] = '/home/admin/Dropbox/GIS/Gen-1/Chennai'
pathGIS[[3]] = '/home/admin/Dropbox/GIS/Gen-1/Chennai'
pathGIS[[4]] = '/home/admin/Dropbox/GIS/Gen-1/PhnomPenh'
pathGIS[[5]] = '/home/admin/Dropbox/GIS/Gen-1/PhnomPenh'
pathGIS[[6]] = '/home/admin/Dropbox/GIS/Gen-1/Tuas'
pathGIS[[7]] = '/home/admin/Dropbox/GIS/Gen-1/Tuas'
pathGIS[[8]] = '/home/admin/Dropbox/GIS/Gen-1/Cebu'

tiltAz[[1]] = '5t_270az'
tiltAz[[2]] = '10t_180az'
tiltAz[[3]] = '10t_0az'
tiltAz[[4]] = '10t_90az'
tiltAz[[5]] = '10t_270az'
tiltAz[[6]] = '10t_180az'
tiltAz[[7]] = '10t_180az'
tiltAz[[8]] = '10t_180az'

filename[[1]] = paste("NewDelhi_711_",tiltAz[[1]],".txt",sep="")
filename[[2]] = paste("Chennai_712_",tiltAz[[2]],".txt",sep="")
filename[[3]] = paste("Chennai_712_",tiltAz[[3]],".txt",sep="")
filename[[4]] = paste("PhnomPenh_714_",tiltAz[[4]],".txt",sep="")
filename[[5]] = paste("PhnomPenh_714_",tiltAz[[5]],".txt",sep="")
filename[[6]] = paste("Tuas_716_",tiltAz[[6]],".txt",sep="")
filename[[7]] = paste("Tuas_717_",tiltAz[[7]],".txt",sep="")
filename[[8]] = paste("Cebu_719_",tiltAz[[8]],".txt",sep="")

idxSERIS[[1]] = c(6,8)
idxSERIS[[2]] = c(3,4,5,6,7,8,9)
idxSERIS[[3]] = c(3,4,5,6,7,8,9)
idxSERIS[[4]] = c(3,4,5,7,8,9,10)
idxSERIS[[5]] = c(3,3,5,7,8,9,10)
idxSERIS[[6]] = c(3)
idxSERIS[[7]] = c(3)
idxSERIS[[8]] = c(3,4,5)


clnames[[1]] = c("Hamb-SERIS","TMod-SERIS")
clnames[[2]] = c("GSi-SERIS","Gmod10N-SERIS","Gmod10S-SERIS","Hamb-SERIS","Tamb-SERIS","TMod10N-SERIS","TMod10S-SERIS")
clnames[[3]] = c("GSi-SERIS","Gmod10N-SERIS","Gmod10S-SERIS","Hamb-SERIS","Tamb-SERIS","TMod10N-SERIS","TMod10S-SERIS")
clnames[[4]] = c("GSI-SERIS","Gmod10E-SERIS","Gmod10W-SERIS","Hamb-SERIS","Tamb-SERIS","TMod10E-SERIS","TMod10W-SERIS")
clnames[[5]] = c("GSI-SERIS","Gmod10E-SERIS","Gmod10W-SERIS","Hamb-SERIS","Tamb-SERIS","TMod10E-SERIS","TMod10W-SERIS")
clnames[[6]] = c("GSI-SERIS")
clnames[[7]] = c("GSI-SERIS")
clnames[[8]] = c("Gpyr-SERIS","GSI-SERIS","Gmod-SERIS")

startGIS[[1]] = 2
startGIS[[2]] = 8
startGIS[[3]] = 8
startGIS[[4]] = 7
startGIS[[5]] = 7
startGIS[[6]] = 2
startGIS[[7]] = 6
startGIS[[8]] = 11

yroffset = c(1,1,1,1,1,2,2,2)

idxGIS = c(2,3,8,9,10,11)

pathwrite = '/home/admin/Dropbox/GIS/GISvsSERIS'
if(!file.exists(pathwrite))
	dir.create(pathwrite)

for(outer in 1 : 1)
{
idxGlobal = 1
idxPrev = 0

TIME = c()
GHIGIS = c()
DIFGIS = c()
GMODGIS = c()
ZENGIS=c()
INCIGIS = c()
SBGIS = c()

GSISERIS = c()
SMPSERIS = c()
GMODSERIS = c()
GMOD2SERIS = c()
TMSERIS = c()
TMod1SERIS = c()
TMod2SERIS = c()

years = dir(pathGIS[[outer]])
for(x in yroffset[outer] : length(years))
{
	pathSERISMonth = paste(pathSERIS[[outer]],years[x],sep="/")
	pathGISMonth = paste(pathGIS[[outer]],years[x],sep="/")
	months = dir(pathGISMonth)
	if(idxGlobal == 1)
		months = months[startGIS[[outer]]:length(months)]
	for(y in 1 : length(months))
	{
		pathGISdays = paste(pathGISMonth,months[y],tiltAz[[outer]],sep="/")
		pathSERISdays = paste(pathSERISMonth,months[y],sep="/")
		daysGIS = dir(pathGISdays)
		daysSERIS = dir(pathSERISdays)
		if(months[y]=="2017-11" || months[y] == "2017-12")
			break
		if((outer == 2 || outer==3) && x==1 && y == length(months))
		{
			daysGIS = daysGIS[-13]
		}
		if((outer == 4 || outer==5) && months[y] == "2016-08")
		{
			daysGIS = daysGIS[-7]
		}
		for(z in 1:length(daysGIS))
		{
			pathSERISData = paste(pathSERISdays,daysSERIS[z],sep="/")
			pathGISData = paste(pathGISdays,daysGIS[z],sep="/")
			datareadGIS = read.table(pathGISData,header = T,sep="\t")
			datareadSERIS = read.table(pathSERISData,header = T,sep = "\t")
			newData = try(cleanData(datareadSERIS,outer),silent=T)
			if(class(newData)=="try-error")
				next
			len = length(as.character(newData[,1]))
			TIME[idxGlobal:(idxPrev+len)] = as.character(newData[,1])
			GSISERIS[idxGlobal:(idxPrev+len)] = as.numeric(newData[,2])

			if(length(idxSERIS[[outer]]) > 1)
				SMPSERIS[idxGlobal:(idxPrev+len)] = as.numeric(newData[,3])
			if(length(idxSERIS[[outer]]) > 2)
				GMODSERIS[idxGlobal:(idxPrev+len)] = as.numeric(newData[,4])
			if(length(idxSERIS[[outer]]) > 3)
				GMOD2SERIS[idxGlobal:(idxPrev+len)] = as.numeric(newData[,5])
			
			if(length(idxSERIS[[outer]]) > 4)
				TMSERIS[idxGlobal:(idxPrev+len)] = as.numeric(newData[,6])
			if(length(idxSERIS[[outer]]) > 5)
				TMod1SERIS[idxGlobal:(idxPrev+len)] = as.numeric(newData[,7])
			if(length(idxSERIS[[outer]]) > 6)
				TMod2SERIS[idxGlobal:(idxPrev+len)] = as.numeric(newData[,8])
			
			GHIGIS[idxGlobal:(idxPrev+len)] = as.numeric(datareadGIS[,idxGIS[1]])
			DIFGIS[idxGlobal:(idxPrev+len)] = as.numeric(datareadGIS[,idxGIS[2]])
			GMODGIS[idxGlobal:(idxPrev+len)] = as.numeric(datareadGIS[,idxGIS[3]])
			ZENGIS[idxGlobal:(idxPrev+len)] = as.numeric(datareadGIS[,idxGIS[4]])
			INCIGIS[idxGlobal:(idxPrev+len)] = as.numeric(datareadGIS[,idxGIS[5]])
			SBGIS[idxGlobal:(idxPrev+len)] = as.numeric(datareadGIS[,idxGIS[6]])
			
			idxGlobal = idxGlobal + len
			idxPrev = idxGlobal - 1
			print(paste(daysGIS[z],daysSERIS[z],"Done"))
		}
	}
}
if(outer > 1 && outer < 6)
{
	GSISERIS = GMOD2SERIS
	GMOD2SERIS=GMODSERIS
	GMODSERIS=SMPSERIS
}
	df = data.frame(Tm = TIME,HambSERIS =GSISERIS,TModSERIS=SMPSERIS,GHIGIS =GHIGIS,DIFGIS=DIFGIS,GMODGIS=GMODGIS,ZENGIS=ZENGIS,INCIGIS=INCIGIS,SBGIS=SBGIS)
if(0){df = data.frame(Tm = TIME, GMODSERIS1=GMODSERIS, GMODSERIS=GMOD2SERIS, GMODGIS=GMODGIS,GHIGIS =GHIGIS, DIFGIS=DIFGIS,ZENGIS=ZENGIS,INCIGIS=INCIGIS,SBGIS=SBGIS,HambSERIS=GSISERIS,TambSERIS=TMSERIS,TMod1SERIS=TMod1SERIS,TMod2SERIS=TMod2SERIS)
if(length(idxSERIS[[outer]]) > 2)
	df = data.frame(Tm = TIME,GSi = GSISERIS, c2 =SMPSERIS, c3 = GMODSERIS,GHIGIS =GHIGIS,DIFGIS=DIFGIS,GMODGIS=GMODGIS,ZENGIS=ZENGIS,INCIGIS=INCIGIS,SBGIS=SBGIS)
if(length(idxSERIS[[outer]]) > 3)
	df = data.frame(Tm = TIME,GSi = GSISERIS, c2 =SMPSERIS, c3=GMODSERIS,c4=GMOD2SERIS,GHIGIS =GHIGIS,GMODGIS=GMODGIS,ZENGIS=ZENGIS,INCIGIS=INCIGIS,SBGIS=SBGIS)
colnames(df) = c("Tm",clnames[[outer]][1:3],"GHIGIS","DIFGIS","GMODGIS","ZENGIS","INCIGIS","SBGIS",clnames[[outer]][4:7])
}
write.table(df,file = paste(pathwrite,filename[[outer]],sep="/"),row.names=F,col.names=T,sep = "\t",append=F)
}
