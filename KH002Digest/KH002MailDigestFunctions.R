rm(list = ls())
TIMESTAMPSALARM = NULL
TIMESTAMPSALARM2 = NULL
METERCALLED = 0
ltcutoff = .001
SOLARREADINGDENOM =1
CONSTYIELD=63
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
	seq1 = seq(from = 1,to = length(hr),by =2)
	seq2 = seq(from = 2,to = length(hr),by =2)
  min = as.numeric(hr[seq2]) 
  hr = as.numeric(hr[seq1]) * 60
  return((hr + min + 1))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

fetchGSIData = function(date)
{
	pathMain = '/home/admin/Dropbox/Second Gen/[KH-003S]'
	pathMain2 = '/home/admin/Dropbox/Second Gen/[KH-714S]'
	yr = substr(date,1,4)
	mon = substr(date,1,7)
	txtFileName = paste('[KH-003S] ',date,".txt",sep="")
	txtFileName2 = paste('[KH-714S] ',date,".txt",sep="")
	pathyr = paste(pathMain,yr,sep="/")
	pathyr2 = paste(pathMain2,yr,sep="/")
	gsiVal = NA
	gsiVal2 = NA
	if(file.exists(pathyr))
	{
		pathmon = paste(pathyr,mon,sep="/")
		if(file.exists(pathmon))
		{
			pathfile = paste(pathmon,txtFileName,sep="/")
			if(file.exists(pathfile))
			{
				dataread = read.table(pathfile,sep="\t",header = T)
				gsiVal = as.numeric(dataread[,4])
			}
		}
	}
	if(file.exists(pathyr2))
	{
		pathmon2 = paste(pathyr2,mon,sep="/")
		if(file.exists(pathmon2))
		{
			pathfile2 = paste(pathmon2,txtFileName2,sep="/")
			if(file.exists(pathfile2))
			{
				dataread2 = read.table(pathfile2,sep="\t",header = T)
				gsiVal2 = as.numeric(dataread2[,6])
			}
		}
	}
	return(c(gsiVal,gsiVal2))
}

secondGenData = function(filepath,writefilepath)
{
  {
		if(METERCALLED == 1){
	 		TIMESTAMPSALARM <<- NULL}
		else if(METERCALLED == 2){
			TIMESTAMPSALARM2 <<- NULL}
	}
  dataread = read.table(filepath,header = T,sep = "\t")
	dataread2 = dataread
	lastread2 = "NA"
	if(ncol(dataread) < 9)
	{
		return(NA)
	}
	dataread = dataread[complete.cases(dataread[,3]),]
	if(nrow(dataread) > 0)
	{
	lastread2 = as.character(dataread[nrow(dataread),3])
	}
	dataread = dataread2[complete.cases(dataread2[,2]),]
	lasttime = as.character(dataread[nrow(dataread),1])
	lastread = as.character(dataread[nrow(dataread),2])
	Eac2 = format(round(as.numeric(dataread[nrow(dataread),2]) - as.numeric(dataread[1,2]),1),nsmall=1)
	dataread = dataread2[complete.cases(dataread2[,9]),]
	dataread = dataread[as.numeric(dataread[,9]) > 0,]
	Eac1 = 0
	if(nrow(dataread) > 1)
	{
   	Eac1 = format(round(sum(as.numeric(dataread[,9]))/12,1),nsmall=1)
	}
	dataread = dataread2[complete.cases(dataread2[,9]),]
	dataread = dataread[as.numeric(dataread[,9]) < 0,]
	Eac22 = 0
	if(nrow(dataread) > 1)
	{
	  Eac22 = format(round(sum(as.numeric(dataread[,9]))/12,1),nsmall=1)
	}
	dataread = dataread2
	dataread = dataread[complete.cases(dataread[,3]),]
	Eac3 = format(round(as.numeric(dataread[nrow(dataread),3]) - as.numeric(dataread[1,3]),1),nsmall=1)
	dataread = dataread2
  DA = format(round(nrow(dataread)/2.88,1),nsmall=1)
  tdx = timetomins(substr(dataread[,1],12,16))
  dataread2 = dataread[tdx > 540,]
  tdx = tdx[tdx > 540]
  dataread2 = dataread2[tdx < 1020,]
	dataread2 = dataread2[complete.cases(dataread2[,9]),]
  missingfactor = 120 - nrow(dataread2)
  dataread2 = dataread2[as.numeric(dataread2[,9]) < ltcutoff,]
	if(length(dataread2[,1]) > 0)
	{
		{
			if(METERCALLED == 1){
				TIMESTAMPSALARM <<- as.character(dataread2[,1])}
			else if(METERCALLED == 2){
		 	 TIMESTAMPSALARM2 <<- as.character(dataread2[,1])}
		}
	}
  totrowsmissing = format(round((missingfactor + nrow(dataread2))/1.2,1),nsmall=1)
	date = substr(as.character(dataread[1,1]),1,10)
	gsiVal = fetchGSIData(date)
	gsiValPyr = gsiVal[2]
	gsiVal = gsiVal[1]
	dspyield = round((as.numeric(Eac2)/CONSTYIELD),2)
	dspyield1 = round((as.numeric(Eac1)/CONSTYIELD),2)
	PR = round((dspyield*100/as.numeric(gsiVal)),1)
	PR1 = round((dspyield1*100/as.numeric(gsiVal)),1)
	PR2 = round((dspyield*100/as.numeric(gsiValPyr)),1)
	{
	  if(METERCALLED == 1)
	  {
		  SOLARREADINGDENOM <<- as.numeric(Eac2)
      df = data.frame(Date = date, SolarPowerGen = as.numeric(Eac1),LoadConsumed = as.numeric(Eac2),
                  LoadDelivered = as.numeric(Eac22),EnergyRecv=as.numeric(Eac3),DA = DA,Downtime = totrowsmissing,
									LastTime = lasttime, LastReadDel = lastread,LastReadRecv=lastread2,GSI=gsiVal,DailySpecYield=dspyield,PR=PR,PyrVal = gsiValPyr,PR2 = PR2,DailySpecYield1=dspyield1,
									PR1=PR1,stringsAsFactors = F)
    } 
	  else if(METERCALLED == 2)
	  {
			percpump = round((SOLARREADINGDENOM*100/(SOLARREADINGDENOM+as.numeric(Eac3))),1)
      df = data.frame(Date = date, EnergyDelivered = as.numeric(Eac1),LoadConsumed = as.numeric(Eac2),
                  LoadDelivered = as.numeric(Eac22),EnergyRecv = as.numeric(Eac3),DA = DA,Downtime = totrowsmissing,
									LastTime = lasttime, LastReadDel = lastread2,LastReadRecv=lastread2 ,PercPumpedIn= percpump,GSI=gsiVal,DailySpecYield=dspyield,PR=PR,PyrVal = gsiValPyr,PR2 = PR2,
									DailySpecYield=dspyield1,PR1=PR1,stringsAsFactors = F)
	  }
	}
	write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  return(df)
}

thirdGenData = function(filepathm1, filepathm2,writefilepath)
{
  dataread2 =read.table(filepathm1,header = T,sep="\t") #its correct dont change
  dataread1 = read.table(filepathm2,header = T,sep = "\t") #its correct dont change
	dt = as.character(dataread1[,7])
	da = as.character(dataread1[,6])
	irr = dataread1[,11]
	EacLCons = as.numeric(dataread2[,3])
	EacLDel = as.numeric(dataread2[,4])
	EnergyDel = as.numeric(dataread2[,2])
	EnergyRecv = as.numeric(dataread2[,5])
	SolarEnergy = as.numeric(dataread1[,2])
  PercPumpedIn = round((EnergyRecv*100)/SolarEnergy,1)
	SolarPowerCons = EnergyRecv - SolarEnergy
	TotPowerCons = SolarPowerCons + EacLCons
	PR = dataread2[,14]
	DSPY = dataread1[,12]
	DSPY2 = dataread2[,13]
	lt = as.character(dataread1[,8])
	lr = as.character(dataread1[,9])
	lr2 = as.character(dataread2[,9])
  df = data.frame(Date = substr(as.character(dataread1[1,1]),1,10), DA= da,Downtime = dt,Irradiance = irr,EacLoadCons = EacLCons,
	EacLoadDelv = EacLDel,EnergyDelv=EnergyDel,EnergyRecv = EnergyRecv,SolarEnergy=SolarEnergy,PercPumpedIn=PercPumpedIn,
	SolarPowerCons = SolarPowerCons,TotPowerCons=TotPowerCons,PR=PR,LastTime = lt,lastRead = lr,LastReadLoad=lr2,DailySpecYieldSol=DSPY,DailySpecYieldLoad=DSPY2,stringsAsFactors=F)
  {
    if(file.exists(writefilepath))
    {
	data = read.table(writefilepath,header=T,sep="\t",stringsAsFactors=F)
	dates = as.character(data[,1])
	idxmtch = match(substr(as.character(dataread1[1,1]),1,10),dates)
	if(is.finite(idxmtch))
	{
		data = data[-idxmtch,]
		write.table(data,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
	}
      write.table(df,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
    }
    else
    {
      write.table(df,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
    }
  }
}

