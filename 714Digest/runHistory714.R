rm(list = ls(all = T))

checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}
prepareSumm = function(dataread)
{
  da = nrow(dataread)
  thresh = 5
  gsi1 = sum(dataread[complete.cases(dataread[,3]),3])/60000
  gsi2 = sum(dataread[complete.cases(dataread[,4]),4])/60000
	gsi3 = sum(dataread[complete.cases(dataread[,5]),5])/60000
  gsismp = sum(dataread[complete.cases(dataread[,6]),6])/60000
  subdata = dataread[complete.cases(dataread[,3]),]
  subdata = subdata[as.numeric(subdata[,3]) > thresh,]
  tamb = mean(dataread[complete.cases(dataread[,8]),8])
  tambst = mean(subdata[,8])
  
  hamb = mean(dataread[complete.cases(dataread[,7]),7])
  hambst = mean(subdata[,7])
  
  tambmx = max(dataread[complete.cases(dataread[,8]),8])
  tambstmx = max(subdata[,8])
  
  hambmx = max(dataread[complete.cases(dataread[,7]),7])
  hambstmx = max(subdata[,7])
  
  tambmn = min(dataread[complete.cases(dataread[,8]),8])
  tambstmn = min(subdata[,8])
  
  hambmn = min(dataread[complete.cases(dataread[,7]),7])
  hambstmn = min(subdata[,7])
  
  tsi1 = mean(dataread[complete.cases(dataread[,3]),9])
  tsi2 = mean(dataread[complete.cases(dataread[,3]),10])
  
  gsirat1 = gsi2 / gsi1
	gsirat2 = gsi3 / gsi1
  smprat = gsismp / gsi2
	daPerc = da/14.4

  datawrite = data.frame(Date = substr(dataread[1,1],1,10),PtsRec = rf(da),Gsi = rf(gsi1), GmodE = rf(gsi2),GmodW = rf(gsi3),Smp = rf(gsismp),
                         Tamb = rf(tamb), TambSH = rf(tambst),TambMx = rf(tambmx), TambMn = rf(tambmn),
                         TambSHmx = rf(tambstmx), TambSHmn = rf(tambstmn), Hamb = rf(hamb), HambSH = rf(hambst),
                         HambMx = rf(hambmx), HambMn = rf(hambmn), HambSHmx = rf(hambstmx), HambSHmn = rf(hambstmn),
                         Tsi01 = rf(tsi1), Tsi02= rf(tsi2), GModERat = rf(gsirat1), GModWRat = rf(gsirat2),SpmRat = rf(smprat),DA=rf(daPerc))
  datawrite
}
rewriteSumm = function(datawrite)
{
  
  df = data.frame(Date = as.character(datawrite[1,1]),Gsi = as.character(datawrite[1,3]),GModE = as.character(datawrite[1,4]),GModW = as.character(datawrite[1,5]),Smp = as.character(datawrite[1,6]),
             Tamb = as.character(datawrite[1,7]),TambSH = as.character(datawrite[1,8]),Hamb = as.character(datawrite[1,13]),HambSH = as.character(datawrite[,14]),
						 DA = as.character(datawrite[1,24]),stringsAsFactors=F)
  df
}

path = "/home/admin/Dropbox/Cleantechsolar/1min/[714]"
pathwrite = "/home/admin/Dropbox/Second Gen/[KH-714S]"
checkdir(pathwrite)
years = dir(path)
x=y=z=1
for(x in 1 : length(years))
{
  pathyear = paste(path,years[x],sep="/")
  writeyear = paste(pathwrite,years[x],sep="/")
  checkdir(writeyear)
  months = dir(pathyear)
  for(y in  1: length(months))
  {
    pathmonth = paste(pathyear,months[y],sep="/")
    writemonth = paste(writeyear,months[y],sep="/")
    checkdir(writemonth)
    days = dir(pathmonth)
    sumfilename = paste("[KH-714S] ",substr(months[y],3,4),substr(months[y],6,7),".txt",sep="")
    for(z in 1 : length(days))
    {
      dataread = read.table(paste(pathmonth,days[z],sep="/"),sep="\t",header = T)
      datawrite = prepareSumm(dataread)
      datasum = rewriteSumm(datawrite)
			currdayw = gsub("714","KH-714S",days[z])
      write.table(datawrite,file = paste(writemonth,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      {
        if(!file.exists(paste(writemonth,sumfilename,sep="/")) || (x == 1 && y == 1 && z==1))
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
        }
        else 
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
        }
      }
    }
  }
}
