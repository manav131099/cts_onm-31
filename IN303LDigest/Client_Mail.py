import pandas as pd
import os
from openpyxl import load_workbook
import xlsxwriter
import smtplib
import numpy as np
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import sys

date = sys.argv[1]

def send_mail(date, recipients, attachment_path_list=None):
    server = smtplib.SMTP("smtp.office365.com")
    server.starttls()
    server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    msg = MIMEMultipart()
    sender = 'operations@cleantechsolar.com'
    dates = pd.to_datetime(date).strftime("%Y-%m-%d")
    msg['Subject'] = "Station [IN-9303L] Client Digest "+ str(dates)
    #if sender is not None:
    msg['From'] = sender
    msg['To'] = ", ".join(recipients)
    # text = "Dear Sir,\n\n Please find the Generation Data for "+str(dates)+"\n\nNote- You can also access the day wise generation data in the following link which is updated daily by 9-AM.\n<a href=""https://rb.gy/32thws"">Energy Generation</a>\n"
    email_body = "Dear Sir,\n\nPlease find the Generation Data for "+str(dates)+".\n\nRegards,\nTeam Cleantech\n "
    if attachment_path_list is not None:
        for each_file_path in attachment_path_list:
            # try:
            file_name = each_file_path.split("/")[-1]
            part = MIMEBase('application', "octet-stream")
            part.set_payload(open(each_file_path, "rb").read())
            encoders.encode_base64(part)
            part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
            msg.attach(part)
        #   except:
                # print("could not attache file")

    msg.attach(MIMEText(email_body))
    server.sendmail(sender, recipients, msg.as_string())
recipients = ['om-it-digest@cleantechsolar.com','operationsCentralIN@cleantechsolar.com']
#recipients = ['ManojNB@exide.co.in','MishraVS@exide.co.in','Shrimant.Kharade@exide.co.in','RohitY@exide.co.in','sab@exide.co.in','om-it-digest@cleantechsolar.com','operationsCentralIN@cleantechsolar.com']
path = '/home/admin/Dropbox/Third Gen/[IN-303L]/'
path_write = '/home/admin/Exide Generation Data.xlsx'
workbook = xlsxwriter.Workbook(path_write)
workbook.close()
mfms = os.listdir(path)[:-1]
df = pd.DataFrame()
mfm_1 = os.listdir(path+'MFM_2_ICR 01 HT MFM')
mfm_2 = os.listdir(path+'MFM_3_ICR 02 HT MFM')
mfm_3 = os.listdir(path+'MFM_4_ICR 03 HT MFM')

months = os.listdir(path+'MFM_2_ICR 01 HT MFM/')
for i in months:
    book = load_workbook(path_write)
    writer = pd.ExcelWriter(path_write, engine = 'openpyxl')
    writer.book = book
    sheet_name = pd.to_datetime(i[:-4]).strftime("%B %y")
    tdf = pd.read_csv(path+'MFM_2_ICR 01 HT MFM/'+i,sep='\t')
    df['Date'] = tdf['Date']
    df['ICR-1 Exide Chinchwad'] = round(tdf['Eac2'],0).astype('int64')

    tdf = pd.read_csv(path+'MFM_3_ICR 02 HT MFM/'+i,sep='\t')
    df['ICR-2 Exide Ahmednagar'] = round(tdf['Eac2'],0).astype('int64')

    tdf = pd.read_csv(path+'MFM_4_ICR 03 HT MFM/'+i,sep='\t')
    df['ICR-3 Exide Taloja'] = round(tdf['Eac2'],0).astype('int64')

    df['Total'] = df['ICR-1 Exide Chinchwad'] + df['ICR-2 Exide Ahmednagar'] + df['ICR-3 Exide Taloja']

    finalrow = pd.DataFrame({'Date':'Total','ICR-1 Exide Chinchwad':df['ICR-1 Exide Chinchwad'].sum(),'ICR-2 Exide Ahmednagar':df['ICR-2 Exide Ahmednagar'].sum(),'ICR-3 Exide Taloja':df['ICR-3 Exide Taloja'].sum(),'Total':df['Total'].sum()},index=[0]) 
    df = pd.concat([df,finalrow],axis=0)
    

    df['ICR-1 Exide Chinchwad']=df['ICR-1 Exide Chinchwad'].apply(lambda x: "{:,}".format(x))
    df['ICR-2 Exide Ahmednagar']=df['ICR-2 Exide Ahmednagar'].apply(lambda x: "{:,}".format(x))
    df['ICR-3 Exide Taloja']=df['ICR-3 Exide Taloja'].apply(lambda x: "{:,}".format(x))
    df['Total']=df['Total'].apply(lambda x: "{:,}".format(x))
    print(df)
    df.to_excel(writer,sheet_name=sheet_name,index=None)
    
    try:
        std=book.get_sheet_by_name('Sheet1')
        book.remove_sheet(std)
        print(book.get_sheet_names())
    except:
        pass
    writer.save()
    writer.close()

attachement = []
attachement.append(path_write)
send_mail(date,recipients,attachement)