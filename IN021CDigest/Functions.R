require('httr')
source('/home/admin/CODE/MasterMail/timestamp.R')
cleansedata = function(pth) #data cleaning for eradicating missing and NA values
{
	df = read.table(pth,header = T,sep="\t")
  c2 = as.numeric(df[,17])
	c4 = as.numeric(df[,16])
	c10 = as.character(df[,10])
	c1idx = which(c10 %in% "n")
  c2idx = which(c2 %in% 0)
	c1 = as.character(df[,19])
  c2 = as.character(df[,18])
  c3 = as.character(df[,17])
	c4 = as.character(df[,16])
	c5 = as.character(df[,15])
  {
	if(length(c2idx) > 1)
	{
	c1 = as.character(df[-(c2idx),19])
  c2 = as.character(df[-(c2idx),18])
  c3 = as.character(df[-(c2idx),17])
  }
	else
	{
	 print('Error c1,c2,c3 has no data')
	 c1=c2=c3=NA
	}
	}
	print(paste('Length of c1 is',length(c1)))
	{
	if(length(c1idx) > 1)
	{
  c4 = as.character(df[c1idx,16])
  c5 = as.character(df[c1idx,15])
	Time = as.character(df[c1idx,19])
	{
	if(length(Time) > length(c1))
	{
		print(paste('Length missmatch c1 length',length(c1),'c4 length',length(c4)))
		index = match(c1,Time)
		idxNAs = which(index %in% NA)
		print(paste('length of idxnas',length(idxNAs)))
		print(paste('length of index prior to cleanup',length(index)))
		index = index[complete.cases(index)]
		print(paste('length of index after cleanup',length(index)))
		c4 = c4[index]
		c5 = c5[index]
		print(paste('Length before cleansing, c1:',length(c1),'c4:',length(c4)))
		print(paste('idxas is',idxNAs))
		if(length(idxNAs))
		{
		for(inner in 1 : length(idxNAs))
		{
			oldlen = length(c4)
			if(idxNAs[inner] <= oldlen)
			{
				c4[(idxNAs[inner]+1) : (oldlen+1)] = c4[idxNAs[inner] : oldlen]
				c5[(idxNAs[inner]+1) : (oldlen+1)] = c5[idxNAs[inner] : oldlen]
			}
			c4[idxNAs[inner]] = c5[idxNAs[inner]] = NA
		}
		  #c4 = c4[1:length(c1)]
			#c5 = c5[1:length(c1)]
		}
		print(paste('Length after cleansing, c1:',length(c1),'c4:',length(c4)))
	}
	else if (length(Time) < length(c1))
	{
		print(paste('Length missmatch c1 length',length(c1),'c4 length',length(c4)))
		index = match(c1,Time)
		print(paste('index length prior to cleanup',length(index)))
		index = index[complete.cases(index)]
		idxactual = c(1:length(c1))
		idxactual = idxactual[-index]
		print(paste('index length:',length(index),'idxactual length:',length(idxactual)))
		c4[idxactual]=NA
		c5[idxactual]=NA
		print(paste('Length after cleansing, c1:',length(c1),'c4:',length(c4)))
	}
	else
		print('Success c1 and c4 have same length')
	}
	}
	else
	{
		c4 = c5=unlist(rep(NA,length(c1)))
		print('Reverting to default')
	}
	}
	print(paste('c1 length is',length(c1),'c4 length is',length(c4)))
	df2 = data.frame(Tm=c1,Pac=c2,Eac=c3,Irr=c4,Tmod=c5)
	recordTimeMaster("IN-021T","Gen1",as.character(df2[nrow(df2),1]))
  return(df2)
}
pathCentral = '/home/admin/Data/TORP Data/AllStations'

probePath = function(day) #probing for current day
{
	path = pathCentral
	DDMMYYYY = unlist(strsplit(as.character(day),"/"))
	pathYr = paste(path,DDMMYYYY[3],sep="/")
	#checkDir(pathYr)
	pathMon = paste(pathYr,paste(DDMMYYYY[3],"-",DDMMYYYY[2],sep=""),sep="/")
	#checkDir(pathYr)
	pathFinal = paste(pathMon,paste(paste(DDMMYYYY[3],DDMMYYYY[2],DDMMYYYY[1],sep="-"),".txt",sep=""),sep="/")
	print(paste('path final is',pathFinal))
	{
		if(file.exists(pathFinal))
		{
			df = try(read.table(pathFinal,header = T,sep = "\t"),silent=T)
			if(class(df)=="try-error")
				df = NULL
			return(df)
		}
		else
			return(NULL)
	}
}

fetchrawdata = function(day1,day2)  #fetching data from Allstations with station name as ATL Limda
{
  	#fetch data from SERVER USING A POST-request
		masterData = probePath(day1)
		
		if(is.null(masterData))
		{
			return(masterData)
		}
		stnnames = as.character(masterData[,13])
		subst = which(stnnames %in% "ATL Limda") 
		if(length(subst))
		{
			vals2 = masterData[subst,]    # extract only rows with MJ Logistics tag in column 13
			recordTimeMaster("IN-021T","FTPNewFiles",as.character(vals2[nrow(vals2),19]))
			return(vals2)
		}
		return(NULL)
}

