errHandle = file('/home/admin/Logs/LogsSG007Mail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
source('/home/admin/CODE/SGDigest/SG007Digest/HistoricalAnalysis2G3GSG007.R')
source('/home/admin/CODE/Misc/memManage.R')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
source('/home/admin/CODE/SGDigest/SG007Digest/aggregateInfo.R')

initDigest = function(df)
{
	body=""
	
	{
	body = paste(body,"Site Name: CP Meiji",sep="")
	body = paste(body,"\n\nLocation: 26 Tuas Avenue 12, Singapore 639043")
	body = paste(body,"\n\nO&M Code: SG-007")
	body = paste(body,"\n\nSystem Size: 167.7 kWp")
	body = paste(body,"\n\nNumber of Energy Meters: 1")
	body = paste(body,"\n\nModule Brand / Model / Nos: 516 / Trina TSM PE14H-325")
	body = paste(body,"\n\nInverter Brand / Model / Nos: 3 / SMA Solid-Q 50")
	body = paste(body,"\n\nSite COD: 2010-01-02")
	body = paste(body,"\n\nSystem age [days]:",as.character((as.numeric(DAYSALIVE)+34)))
	body = paste(body,"\n\nSystem age [years]:",as.character(round((as.numeric(DAYSALIVE)+34)/365,1)))
  }
	body = paste(body,"\n\n______________________________________________\n\n")
  body = paste(body,as.character(df[,1]))
  body = paste(body,"\n\n______________________________________________")
  body = paste(body,"\n\nEac-1 [kWh]: ",as.character(df[,2]),sep="")
  body = paste(body,"\n\nEac-2 [kWh]: ",as.character(df[,3]),sep="")
	body = paste(body,"\n\nYield-1 [kWh/kWp]: ",as.character(df[,8]),sep="")
	body = paste(body,"\n\nYield-2 [kWh/kWp]: ",as.character(df[,9]),sep="")
	body = paste(body,"\n\nIrradiance from SG-003S [kWh/m2]: ",as.character(df[,12]),sep="")
	body = paste(body,"\n\nPR-1 [%]: ",as.character(df[,10]),sep="")
	body = paste(body,"\n\nPR-2 [%]: ",as.character(df[,11]),sep="")
  acpts = round(as.numeric(df[,4]) * 14.4)
  body = paste(body,"\n\nPoints recorded: ",acpts," (",as.character(df[,4]),"%)",sep="")
  body = paste(body,"\n\nDowntime (%):",as.character(df[,5]))
  body = paste(body,"\n\nLast recorded timestamp:",as.character(df[,6]))
	body = paste(body,"\n\nLast recorded energy meter reading [kWh]:",as.character(df[,7]))
  return(body)
}
printtsfaults = function(TS,body)
{
	if(length(TS) > 1)
	{
		body = paste(body,"\n\n______________________________________________\n")
		body = paste(body,paste("\nTimestamps where Pac < 1 between 8am - 6pm\n"))
		for(lm in  1 : length(TS))
		{
			body = paste(body,TS[lm],sep="\n")
		}
		body = paste(body,"\n______________________________________________\n")
	}
	return(body)
}

sendMail = function(df1,pth1,pth3)
{
  filetosendpath = c(pth1)
  if(file.exists(pth3))
	{
	filetosendpath = c(pth1,pth3)
  }
	filenams = c()
  for(l in 1 : length(filetosendpath))
  {
    temp = unlist(strsplit(filetosendpath[l],"/"))
    filenams[l] = temp[length(temp)]
		if(l == 1)
			currday = temp[length(temp)]
  }
	data3G = read.table(pth3,header =T,sep = "\t",stringsAsFactors=F)
	dateineed = unlist(strsplit(filenams[1]," "))
	dateineed = unlist(strsplit(dateineed[2],"\\."))
	dateineed = dateineed[1] 
	idxineed = match(dateineed,as.character(data3G[,1]))
	print('Filenames Processed')
	currday = as.character(df1[1,1])
	GLOBALENERGYDELIV <<- as.character(df1[1,10])
  body = initDigest(df1)  #its correct, dont change
  body = printtsfaults(TIMESTAMPSALARM,body)
	print('2G data processed')
	body = paste(body,"\n\n______________________________________________\n\n")
  body = paste(body,"Station Data")
  body = paste(body,"\n\n______________________________________________\n\n")
  body = paste(body,"Station DOB:",as.character(DOB))
  body = paste(body,"\n\n# Days alive:",as.character(DAYSALIVE))
  yrsalive = format(round((DAYSALIVE/365),2),nsmall=2)
  body = paste(body,"\n\n# Years alive:",yrsalive)
	body = gsub("\n ","\n",body)
	print('3G data processed')
  send.mail(from = sender,
            to = recipients,
            subject = paste("Station [SG-007X] Digest",currday),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = filetosendpath,
            file.names = filenams, # optional parameter
            debug = F)
  recordTimeMaster("SG-007X","Mail",currday)
}
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'

# recipients = getRecipients("SG-007X","m")
#recipients = c('shravan1994@gmail.com')
recipients = c('operationsSG@cleantechsolar.com','om-it-digest@cleantechsolar.com','om-interns@cleantechsolar.com','arunsenthil@cutechgroup.com','rajasekar@cutechgroup.com','amirul@cutechgroup.com')
pwd = 'CTS&*(789'
todisp = 1
while(1)
{
	# recipients = getRecipients("SG-007X","m")
  recipients = c('operationsSG@cleantechsolar.com','om-it-digest@cleantechsolar.com','om-interns@cleantechsolar.com','arunsenthil@cutechgroup.com','rajasekar@cutechgroup.com','amirul@cutechgroup.com')
  recordTimeMaster("SG-007X","Bot")
	sendmail = 0
  prevx = x
  prevy = y
  prevt = t
  years = dir(path)
  for(x in prevx : length(years))
  {
    pathyear = paste(path,years[x],sep="/")
		irrpathyears = paste(irrpath,years[x],sep = "/")
    writepath2Gyr = paste(writepath2G,years[x],sep="/")
    writepath3Gyr = paste(writepath3G,years[x],sep="/")
    checkdir(writepath2Gyr)
    checkdir(writepath3Gyr)
    months = dir(pathyear)
    startmnth = 1
    if(prevx == x)
    {
      startmnth = prevy
    }
    for(y in startmnth : length(months))
    {
      pathmonths = paste(pathyear,months[y],sep="/")
			irrpathmonth = paste(irrpathyears,months[y],sep = "/")
			daysirr = dir(irrpathmonth)
      writepath2Gmon = paste(writepath2Gyr,months[y],sep="/")
      writepath3Gfinal = paste(writepath3Gyr,"/[SG-007X] ",months[y],".txt",sep="")
      checkdir(writepath2Gmon)
      pathdays = pathmonths
      days = dir(pathdays)
      writepath2Gdays = writepath2Gmon
      checkdir(writepath2Gdays)
      startdays = 1
      if(y == prevy)
      {
        startdays = prevt
      }
      if(startdays == 1)
      {
        temp = unlist(strsplit(days[1]," "))
				print(substr(temp[2],1,10))
        temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
      }
      for(t in startdays : length(days))
      {
         condn1  = (t != length(days))
         condn2 = ((t == length(days)) && ((y != length(months)) || (x != length(years))))
         if(!(condn1 || condn2))
         {
				 	 if(todisp)
					 {
					 	print('No new files')
						todisp = 0
					 }
					tmNow = as.character(format(Sys.time(),tz = "Singapore"))
         	writepath2Gfinal = paste(writepath2Gdays,"/",days[t],sep="")
         	readpath = paste(pathdays,days[t],sep="/")
         	df = secondGenData(readpath,writepath2Gfinal)
					tmNow = as.character(format(Sys.time(),tz = "Singapore"))
				 	thirdGenData(writepath2Gfinal,writepath3Gfinal,tmNow)
           Sys.sleep(3600)
           next
         }
				 todisp = 1
         sendmail = 1
         DAYSALIVE = DAYSALIVE + 1
				 print(paste('Processing',days[t]))
         writepath2Gfinal = paste(writepath2Gdays,"/",days[t],sep="")
         readpath = paste(pathdays,days[t],sep="/")
         df = secondGenData(readpath,writepath2Gfinal)
				 tmNow = as.character(format(Sys.time(),tz = "Singapore"))
				 thirdGenData(writepath2Gfinal,writepath3Gfinal,tmNow)
  if(sendmail ==0)
  {
    next
  }
	print('Sending Mail')
  sendMail(df,writepath2Gfinal,writepath3Gfinal)
	print('Mail Sent')
      }
    }
  }
	gc()
}
sink()
