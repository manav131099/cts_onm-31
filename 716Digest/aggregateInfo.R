source('/home/admin/CODE/common/aggregate.R')

registerMeterList("SG-003S",c("M1","M2","M3","InSameFile"))
aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 52 #Column no for DA
aggColTemplate[3] = 38 #column for LastRead
aggColTemplate[4] = 49 #column for LastTime
aggColTemplate[5] = 17 #column for Eac-1
aggColTemplate[6] = 20 #column for Eac-2
aggColTemplate[7] = 32 #column for Yld-1
aggColTemplate[8] = 35 #column for Yld-2
aggColTemplate[9] = 23 #column for PR-1
aggColTemplate[10] = 26 #column for PR-2
aggColTemplate[11] = 3 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = 4 #column for Tamb
aggColTemplate[14] = 16 #column for Tmod
aggColTemplate[15] = 10 #column for Hamb

registerColumnList("SG-003S","M1",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 52 #Column no for DA
aggColTemplate[3] = 39 #column for LastRead
aggColTemplate[4] = 50 #column for LastTime
aggColTemplate[5] = 18 #column for Eac-1
aggColTemplate[6] = 21 #column for Eac-2
aggColTemplate[7] = 33 #column for Yld-1
aggColTemplate[8] = 36 #column for Yld-2
aggColTemplate[9] = 24 #column for PR-1
aggColTemplate[10] = 27 #column for PR-2
aggColTemplate[11] = 3 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = 4 #column for Tamb
aggColTemplate[14] = 16 #column for Tmod
aggColTemplate[15] = 10 #column for Hamb

registerColumnList("SG-003S","M2",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 52 #Column no for DA
aggColTemplate[3] = 40 #column for LastRead
aggColTemplate[4] = 51 #column for LastTime
aggColTemplate[5] = 19 #column for Eac-1
aggColTemplate[6] = 22 #column for Eac-2
aggColTemplate[7] = 34 #column for Yld-1
aggColTemplate[8] = 37 #column for Yld-2
aggColTemplate[9] = 25 #column for PR-1
aggColTemplate[10] = 28 #column for PR-2
aggColTemplate[11] = 3 #column for Irr
aggColTemplate[12] = "Self" # IrrSrc Value
aggColTemplate[13] = 4 #column for Tamb
aggColTemplate[14] = 16 #column for Tmod
aggColTemplate[15] = 10 #column for Hamb

registerColumnList("SG-003S","M3",aggNameTemplate,aggColTemplate)

